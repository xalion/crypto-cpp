# crypto-cpp

### Download on like unix terminal
```
git clone https://gitlab.com/xalion/crypto-cpp.git
cd crypto-cpp 
```

### Compiling and Running with g++
```
g++ -o name_executable_file name source_file
./name_executable_file
```
For example
```
g++ -o rsa rsa.cpp
./rsa
```
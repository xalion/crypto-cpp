#include <iostream>
#include <cmath>
#include <utility>

using namespace std;

int main() {
    int p = 5;
    int q = 7;
    int module = p * q;
    
    int function_of_Euler = (p - 1) * (q - 1);
    int e = 7;
    int d = (function_of_Euler + 1) / e;
    
    pair<int, int> public_key(e, module);
    cout << "1. Alice gives Max the public key\n";
    cout << "Public key Alices: {" <<public_key.first << ", " << public_key.second << "}\n";
    
    pair<int, int> closed_key(d, module);
    cout << "2. Alice keeps the secret key\n";
    cout << "Closed key Alices: {" << closed_key.first << ", " << closed_key.second << "}\n\n";
    
    int msg = 3;
    cout << "Message Maxs: " << msg << "\n";
    
    int encrypted_msg = pow(msg, public_key.first);
    encrypted_msg %= module;
    cout << "3. Max encrypts his message\n";
    cout << "Encrypted message from Maxs: " << encrypted_msg << "\n";
    
    int decrypted_msg = pow(encrypted_msg, public_key.first);
    decrypted_msg %= module;
    cout << "4. Alice decrypts a max message using her private key\n";
    cout << "Decrypted message from Maxs: " << decrypted_msg << "\n\n";
    
    return 0;
}

#include <iostream>
#include <string>
#include <cmath>
#include <cctype>
#include <cstdlib>
#include <fstream>

using namespace std;

short key_generator();
string encoder(string sourse_msg, short key);
string decoder(string encrypt_msg, short key);
void write_to_file_enc(string encrypt_msg);
void write_to_file_dec(string decrypt_msg, string file_name);
string read_from_file(string address_file);

const string bold_text = "\033[1m";
const string stop_bold_text = "\033[0m";


int main() {
    string answer;
    cout << bold_text << "\n\nEnter \"\033[4me\033[0m\" for encode or \"\033[4md\033[0m\" for decode message? → " << stop_bold_text;
    getline(cin, answer);
    
    if (answer == "e") {
        string sourse_msg;
        string encrypt_msg;
        char answer = 0;
        short key = key_generator();
        
        cout << bold_text << "\nEnter message to encrypt: ";
        getline(cin, sourse_msg);
        cout << stop_bold_text;
        
        encrypt_msg = encoder(sourse_msg, key);
        cout << bold_text << "\n\033[38;5;46mEncrypted message: " << encrypt_msg << "\033[0m" << "\n\033[38;5;27mKey for decode:\033[0m " << key << stop_bold_text << endl;
        
        cout << bold_text << "\nDo you want to write the result to a file?\nWrite \033[38;5;46my\033[0m or \033[38;5;196mn\033[0m: ";
        cin >> answer;
        cout << stop_bold_text;
        
        if (answer == 'y') {
            write_to_file_enc(encrypt_msg);
        }
        
    } else if (answer == "d") {
        string sourse_msg;
        string decrypt_msg;
        string file_name;
        short key;
        char answer = 0;
        
        cout << bold_text << "\nPut the file in one directory with the script and write its name: ";
        getline(cin, file_name);
        cout << "\nEnter key for decode: ";
        cin >> key;
        cout << stop_bold_text;
        
        sourse_msg = read_from_file(file_name);
        decrypt_msg = decoder(sourse_msg, key);
        cout << bold_text << "\n\033[38;5;46mDecrypted message: " << decrypt_msg << stop_bold_text << endl;
        
        cout << bold_text << "\nDo you want to overwrite the result in file?\nWrite \033[38;5;46my\033[0m or \033[38;5;196mn\033[0m: ";
        cin >> answer;
        cout << stop_bold_text;
        
        if (answer == 'y') {
            write_to_file_dec(decrypt_msg, file_name);
        }
    }
    
    return 0;
}



short key_generator() {
    srand( static_cast<unsigned int>(time(NULL)) );
    
    return 1 + rand() % 10;
}

string encoder(string sourse_msg, short key) {
    string encrypt_msg;
    
    for (short i = 0; i < sourse_msg.length(); i++) {
        encrypt_msg += sourse_msg[i] + key;
    }
    
    return encrypt_msg;
}

string decoder(string encrypt_msg, short key) {
    string decrypt_msg;
    
    for (short i = 0; i < encrypt_msg.length(); i++) {
        decrypt_msg += encrypt_msg[i] - key;
    }
    
    return decrypt_msg;
}

void write_to_file_enc(string encrypt_msg) {
    ofstream fout("result.txt", ios_base::trunc);
    fout << encrypt_msg << endl;
    fout.close();
    
    if (!fout.is_open()) {
        cout << bold_text << "\033[38;5;203mThe encrypted message in a file \"result.txt\" in the current directory.\n\n\n" << stop_bold_text;
    } else {
        cout << bold_text << "\033[38;5;196mProblems with the file, restart the script\n\n\n" << stop_bold_text;
    }
}

void write_to_file_dec(string decrypt_msg, string file_name) {
    ofstream fout(file_name, ios_base::trunc);
    fout << decrypt_msg << endl;
    fout.close();
    
    if (!fout.is_open()) {
        cout << bold_text << "\033[38;5;203mThe decrypted message in a file \"" << file_name << "\" in the current directory.\n\n\n" << stop_bold_text;
    } else {
        cout << bold_text << "\033[38;5;196mProblems with the file, restart the script\n\n\n" << stop_bold_text;
    }
}

string read_from_file(string address_file) {
    string msg_from_file;
    ifstream reading_file(address_file);
    
    while (!reading_file.eof()) {
        reading_file >> msg_from_file;
    }

    reading_file.close();
    
    return msg_from_file;
}

